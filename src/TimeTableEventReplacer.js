import { ApplicationService, DataNotFoundError, TraceUtils } from '@themost/common';
import { DataObject, EdmMapping, EdmType, ModelClassLoaderStrategy, SchemaLoaderStrategy } from '@themost/data';
import { QueryEntity, QueryExpression, QueryField } from '@themost/query';
import path from 'path';

class TimetableEvent extends DataObject {
    /**
    * @returns {DataQueryable}
    */
      @EdmMapping.func('availableCourseExams', EdmType.CollectionOf('CourseExam'))
      async getCourseExams() {
        const timetableID = this.getId();
        const timetable = await this.context.model('TimetableEvent')
            .where('id')
            .equal(timetableID)
            .expand('studyPrograms','availableEventTypes').getItem();
    
        //should be found only one 
        if (Array.isArray(timetable) && timetable.length === 0 || timetable.lenght > 1) {
          throw new DataNotFoundError('The studyProgram associated with the specified timetable cannot be found or more than one found.')
        }

        const studyProgram = this.context.model('StudyProgram').convert(timetable.studyPrograms[0]);
        return await await studyProgram.getCourseExams();
      }

    /**
    * @returns {DataQueryable}
    */
      @EdmMapping.func('availableCourseClassInstructors', EdmType.CollectionOf('CourseClassInstructor'))
      async getCourseClassInstructors() {
        const timetableID = this.getId();
        const timetable = await this.context.model('TimetableEvent')
            .where('id')
            .equal(timetableID)
            .expand('studyPrograms','availableEventTypes').getItem();

            const studyProgram = this.context.model('StudyProgram').convert(timetable.studyPrograms[0]);
        
            if (!studyProgram) {
              throw new DataNotFoundError('The specified studyProgram cannot be found.')
            }
        
            try {
              /**
               * @type {import('@themost/data').DataQueryable}
               */
              const queryable = this.context.model('CourseClassInstructor').asQueryable();
              queryable
                .select();
              const studyProgramCourseAdapter = this.context.model('StudyProgramCourse').sourceAdapter;
              const StudyProgramCourses = new QueryEntity(studyProgramCourseAdapter).as('StudyProgramCourses0')
              queryable.query.join(StudyProgramCourses).with(
                  new QueryExpression().where(new QueryField('studyProgram').from(StudyProgramCourses))
                .equal(studyProgram.id)
              );
              
              const courseClassesAdapter = this.context.model('CourseClass').sourceAdapter;
              const CourseClasses = new QueryEntity(courseClassesAdapter).as('CourseClasses0')
              queryable.query.join(CourseClasses).with(
                new QueryExpression()
                .where(new QueryField('course').from(CourseClasses))
                .equal(new QueryField('course').from('StudyProgramCourses0'))
                .and(new QueryField('courseClass').from(queryable.query.$collection))
                .equal(new QueryField('id').from(CourseClasses))
              );

              queryable.query
                .prepare()
              return queryable;
            } catch (err) {
              TraceUtils.error(err);
              throw err;
            }
          }
      }
class TimeTableEventReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }


    apply() {
        try {
            // get schema loader
            const schemaLoader = this.getApplication()
                .getConfiguration()
                .getStrategy(SchemaLoaderStrategy);
            // get model definition
            const model = schemaLoader.getModelDefinition('TimetableEvent');
            const findAttribute = model.fields.find((field) => {
                return field.name === 'studyPrograms';
            });
            if (findAttribute == null) {
                model.fields.push({
                    // new added field in model with name, title, description and type
                    name: 'studyPrograms',
                    title: 'Timetable StudyPrograms',
                    description: 'The studyProgram that a timetable belongs to.',
                    type: 'StudyProgram',
                    many: true,
                    mapping: {
                        // juction as associationType, wil create a new table
                        associationType: 'junction', 
                        // new table's name 
                        associationAdapter: 'TimeTableStudyPrograms',
                        // object for field parentId in new table
                        parentModel: 'StudyProgram', 
                        parentField: 'id',
                        // object for field valueId in new table
                        childModel: 'TimetableEvent',
                        childField: 'id',
                        cascade: 'delete',   
                    }
                });
            }

            model.eventListeners = model.eventListeners || [];
            model.eventListeners.push({
                type: path.resolve(__dirname, 'listeners/OnSelectTimetableEvent')
            });

            schemaLoader.setModelDefinition(model);

            const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
            const TimetableEventClass = loader.resolve(model);
            TimetableEventClass.prototype.getCourseExams = TimetableEvent.prototype.getCourseExams;
            TimetableEventClass.prototype.getCourseClassInstructors = TimetableEvent.prototype.getCourseClassInstructors;
            
        } catch (err) {
            TraceUtils.error(
                'An error occured while the LongisService tried to extend the TimeTable model (TimeTableEventReplacer).'
            );
            TraceUtils.error(err);
            throw err;
        }
    }
}

export {
    TimeTableEventReplacer
  }
