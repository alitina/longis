import { DataError } from '@themost/common';
export class  GenericDataConflictError extends DataError {
    /**
     * @param {string} code
     * @param {string=} message
     * @param {string=} innerMessage 
     * @param {string=} model 
     */
    constructor(code, message, innerMessage, model) {
        super(code, message || 'An error occurred while validating the state of the target object.', innerMessage, model);
        this.statusCode = 409;
    }
}

export class  DataConflictError extends GenericDataConflictError {
    /**
     * @param {string=} message 
     * @param {string=} innerMessage 
     * @param {string=} model 
     */
    constructor(message, innerMessage, model) {
        super('E_CONFLICT', message, innerMessage, model);
    }
}