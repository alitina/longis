import { DataModel } from '@themost/data/data-model';

const resolver = {
    studyPrograms: function (callback) {
        const undefinedStudyProgram = -1;
        if (this.context.user == null) {
            return callback(null, undefinedStudyProgram);
        }
        const studyPrograms = this.context.user.studyPrograms;
        if (Array.isArray(studyPrograms)) {
            return callback(null, studyPrograms);
        }
        return this.context.model('User')
            .where('name').equal(this.context.user.name)
            .select('id').expand('studyPrograms')
            .silent()
            .getItem()
            .then((res) => {
                if (res && res.studyPrograms && res.studyPrograms.length > 0) {
                    const studyPrograms = res.studyPrograms.map((x) => {
                        return x.id;
                    });
                    Object.assign(this.context.user, {
                        studyPrograms
                    })
                    return callback(null, studyPrograms);
                }
                return callback(null, undefinedStudyProgram);
            }).catch((err) => {
                return callback(err);
            });
    }
};

const superResolve = DataModel.prototype.resolveMethod;

Object.assign(DataModel.prototype, {
    /**
     * @param {string} name
     * @param {Function} callback
     */
    resolveMethod: function (name, args, callback) {
        const thisResolve = resolver[name];
        if (typeof thisResolve === 'function') {
            const argsWithCallback = args || [];
            argsWithCallback.push(callback);
            return thisResolve.apply(this, argsWithCallback);
        }
        return superResolve.call(this, name, args, callback);
    }
});