
class SelectQueryUtils {
    /**
     * @param {import('@themost/query').QueryExpression} query 
     */
    constructor(query) {
        this.query = query;
    }

    isSelectQuery() {
        // handle only select statements
        if (this.query && this.query.$select) {
            return true;
        }
        return false;
    }

    /**
     * @param {string} entity 
     */
    alreadyJoinedWith(entity) {
        if (this.query.$expand) {
            if (Array.isArray(this.query.$expand)) {
                return this.query.$expand.findIndex((item) => {
                    return item.$entity && (item.$entity.$as === entity || item.$entity.$alias === entity);
                }) >= 0;
            } else {
                if (this.query.$expand.$entity && (this.query.$expand.$entity.$as === entity || this.query.$expand.$entity.$alias === entity)) {
                    return true;
                }
            }
        }
        return false;
    }

}

export {
    SelectQueryUtils
}