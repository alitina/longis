/* eslint-disable no-unused-vars */
import { DataNotFoundError, TraceUtils } from '@themost/common';
import { DataEventArgs } from '@themost/data';
import { ExpressDataContext } from '@themost/express';
/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync() {
  //
}

async function afterSaveAsync(event) {
  /**
   * @type {ExpressDataContext}
   */
  const context = event.model.context;
    /**
   * @type {CourseClass}
   */
  const courseClass = context.model('CourseClass').convert(event.target);

  if (event.state === 1) {
    /**
   * CourseExam
   * @type {CourseExam}
   */
    const examPeriods = await context.model('ExamPeriod').getItems();
    const allowedPeriods = examPeriods.filter(examPeriod => {
      return (courseClass.period && examPeriod.periods) === courseClass.period;
      });
    
    if (!Array.isArray(allowedPeriods) || allowedPeriods.length <= 0) {
      throw new DataNotFoundError('No exam periods were found during which exams are allowed to be created.');
    }
    if (Array.isArray(allowedPeriods) && allowedPeriods.length > 0) {
      for (let i = 0; i< allowedPeriods.length; i++) {
        try {
          await courseClass.createCourseExam({examPeriod: allowedPeriods[i].id, year: courseClass.year })
        } catch (err) {
          TraceUtils.error(err);
          throw err;
        }
      } 
    }
  }
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
  return beforeSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
  afterSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}
