import { QueryExpression, QueryEntity, QueryField } from '@themost/query';
import { UserContext } from '../UserContext';

/**
 * @param {import('@themost/data').DataEventArgs} event 
 * @returns 
 */
async function beforeExecuteAsync(event) {
    // get context
    const context = event.model.context;
    // get user context
    const userContext = new UserContext(context);
    /**
     * @type {QueryExpression}
     */
    const query = event.emitter && event.emitter.query;
    if (query == null) {
        return Promise.resolve();
    }
    // handle only select statements
    if (query.$select == null) {
        return Promise.resolve();
    }
    if (query.$expand) {
        if (Array.isArray(query.$expand)) {
            const alreadyJoined = query.$expand.find((item) => {
                return item.$entity && item.$entity.$as === 'SelectStudyPrograms';
            });
            if (alreadyJoined) {
                return;
            }
        } else {
            if (query.$expand.$entity && query.$expand.$entity.$as === 'SelectStudyPrograms') {
                return;
            }
        }
    }
    if (userContext.scopes.includes('registrar') === false) {
        return;
    }
    // get user groups
    const isRegistrarAssistant = await userContext.is('RegistrarAssistants');
    if (isRegistrarAssistant === false) {
        return;
    }
    // get user
    const user = await userContext.user;
    // join user study programs
    // pseudo SQL: 
    // INNER JOIN UserStudyPrograms ON
    //      UserStudyPrograms.studyProgram = StudyProgramBase.id
    //      AND UserStudyPrograms.user = ${user.id}
    const UserStudyPrograms = new QueryEntity('UserStudyPrograms').as('SelectStudyPrograms');
    const StudyPrograms = new QueryEntity(query.$collection);
    query.join(UserStudyPrograms).with(
        new QueryExpression().from(UserStudyPrograms)
        .where(
            new QueryField('studyProgram').from(UserStudyPrograms)
        ).equal(
            new QueryField('id').from(StudyPrograms)
        ).and(
            new QueryField('user').from(UserStudyPrograms)
        ).equal(
            user.id
        )
    );
}


/**
 * @param {import('@themost/data').DataEventArgs} event 
 * @param {Function} callback 
 * @returns 
 */
function beforeExecute(event, callback) {
    return beforeExecuteAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

export {
    beforeExecute
}