import {FileSchemaLoaderStrategy} from '@themost/data';
import { ConfigurationBase } from '@themost/common';
export declare class EpicurSchemaReplacer extends FileSchemaLoaderStrategy {
    constructor(config: ConfigurationBase);
}