import { ApplicationService } from '@themost/common';
import { ScopeString } from '@universis/janitor';
class GuestUserProvisioningService extends ApplicationService {
    constructor(app) {
        super(app);
    }

    /**
     * @param {DataContext} context
     * @param {{
            scope: string,
            username: string,
            preferred_username: string,
            email: string,
            name: string,
            family_name: string,
            given_name: string
        }} info 
     * @returns {Promise<{id:number,name:string,description:string}>}
     */
    async getUser(context, info) {
        const username = info.preferred_username || info.username;
        let user = await context.model('User').where('name').equal(username).silent().getItem();
        if (user == null) {
            const scopes = new ScopeString(info.scope).split();
            if (scopes.includes('candidates')) {
                // try to add user
                user = await context.model('User').silent().insert({
                    name: username,
                    description: info.name || username,
                    alternateName: info.email,
                    groups: [
                        {
                            name: 'Candidates'
                        }
                    ]
                });
                return user
            }
        }
        return user;
    }
}

export {
    GuestUserProvisioningService
}