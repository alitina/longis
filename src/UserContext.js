
const userProperty = Symbol('user');

function splitScope(str) {
    // the default regular expression includes comma /([\x21\x23-\x5B\x5D-\x7E]+)/g
    // the modified regular expression excludes comma /x2C /([\x21\x23-\x2B\x2D-\x5B\x5D-\x7E]+)/g
    const re = /([\x21\x23-\x2B\x2D-\x5B\x5D-\x7E]+)/g
    const results = [];
    let match = re.exec(str);
    while(match !== null) {
        results.push(match[0]);
        match = re.exec(str);
    }
    return results;
}

class UserContext {
    /**
     * 
     * @param {import('@themost/express').ExpressDataContext} context 
     */
    constructor(context) {
        this.context = context;
    }

    /**
     * @returns {string[]}
     */
    get scopes() {
        if (this.context.user == null) {
            return [];
        }
        if (this.context.user.authenticationScope == null) {
            return [];
        }
        return splitScope(this.context.user.authenticationScope);
    }

    /**
     * @returns {Promise<{ name: string,alternateName: string, enabled: string,groups: Array<{id: number, name: string, alternateName: string}> }>}
     */
    get user() {
        if (this.context.user == null) {
            return Promise.reject(new Error('Expected a valid user context.'));
        }
        const descriptor = Object.getOwnPropertyDescriptor(this.context, userProperty);
        if (descriptor && descriptor.value) {
            return Promise.resolve(descriptor.value);
        }
        return this.context.model('User')
            .where('name').equal(this.context.user.name)
            .expand('groups').getItem().then((user) => {
                Object.defineProperty(this.context, userProperty, {
                    value: user,
                    enumerable: false,
                    configurable: true,
                    writable: true
                });
                return user;
            });
    }

    /**
     * @param {string} group 
     * @returns {Promise<Boolean>}
     */
    async is(group) {
        const user = await this.user;
        return user.groups.findIndex((x) => {
            return x.name === group;
        }) >= 0;
    }

}

export {
    UserContext
}