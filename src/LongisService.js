import { ApplicationService } from '@themost/common';
import  LocalScopeAccessConfiguration from './config/scope.access.json';
import { SpecializationCourseReplacer } from './SpecializationCourseReplacer';
import { TimeTableEventReplacer } from './TimeTableEventReplacer';
import { StudyProgramReplacer } from './StudyProgramReplacer';
import { ODataModelBuilder } from '@themost/data';
import { UserReplacer } from './UserReplacer';
import './DataModelExtensions';
import { CourseReplacer } from './CourseReplacer';
import { CourseClassReplacer } from './CourseClassReplacer';
import { CourseExamReplacer } from './CourseExamReplacer';

const Replacers = [
    SpecializationCourseReplacer,
    StudyProgramReplacer,
    TimeTableEventReplacer,
    UserReplacer,
    CourseReplacer,
    CourseClassReplacer,
    CourseExamReplacer
];
export class LongisService extends ApplicationService {
    constructor(app) {
        // apply replacers
        Replacers.forEach((Replacer) => {
            new Replacer(app).apply();
        });

        super(app);
        // extend universis api scope access configuration
        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // add extra scope access elements
                    const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                    if (scopeAccess != null) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                    }
                }
            });
        }

        /**
         * reset builder
         * @type ODataModelBuilder
         */
        const builder = app.getService(ODataModelBuilder);
        if (builder != null) {
            // cleanup builder and wait for next call
            builder.clean(true);
            builder.initializeSync();
        }

    }

}
