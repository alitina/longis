import { ApplicationService, TraceUtils } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';    

class CourseClassReplacer extends ApplicationService {
    constructor(app) {
        super(app);
        this.model = 'CourseClass'
    }

    apply() {
      try {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition(this.model);
        const findAttribute = model.fields.find((field) => {
          return field.name === 'studyProgram';
        });
        if (findAttribute == null) {
          model.fields.push({
            // new added field in model with name, title, description and type
            name: 'studyProgram',
            title: 'Related studyProgram',
            description: 'The studyProgram that a courseClass belongs to.',
            type: 'StudyProgram',
            multiplicity: 'ZeroOrOne',
            mapping: {
              associationAdapter: 'StudyProgramCourseClasses',
              parentModel: 'CourseClass',
              parentField: 'id',
              childModel: 'StudyProgram',
              childField: 'id',
              associationType: 'junction',
              cascade: 'delete',
              associationObjectField: 'courseClass',
              associationValueField: 'studyProgram'
            }
          });
        }
        if (model) {
            model.eventListeners = model.eventListeners || [];
            // add extra listener
            model.eventListeners.push({
              type: path.resolve(__dirname, 'listeners/OnSelectCourseClass')
            });
            model.eventListeners.push({
              type: path.resolve(__dirname, 'listeners/auto-create-course-exam-listener')
            });
            schemaLoader.setModelDefinition(model);
        }
      } catch (err) {
        TraceUtils.error(
          'An error occured while the LongisService tried to extend (CourseClassReplacer) the CourseClass model.'
        );
        TraceUtils.error(err);
        throw err;
      }
    }

}

export {
    CourseClassReplacer
}
